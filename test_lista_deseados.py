import unittest
from tienda_autos.tienda import Tienda
from tienda_autos.vehiculo import Vehiculo


class PruebaListaDeseados(unittest.TestCase):
    def setUp(self) -> None:
        self.audi = Vehiculo(4, "rojo", 500000, "Audi")
        self.ford = Vehiculo(4, "negro", 460000, "Ford")
        self.lista_deseados = Tienda()
        self.lista_deseados.agregar_elemento_a_lista_deseados(self.audi)

    def tearDown(self) -> None:
        pass

    # assertEqual
    def test_color_audi(self):
        self.assertEqual(self.audi.color, "rojo")

    # assertIs / assertIsNot
    def test_obtener_auto_audi(self):
        item = self.lista_deseados.obtener_elemento(self.audi)
        self.assertIs(item, self.audi)
        self.assertIsNot(item, self.ford)

    # assertRaises
    def test_exception_get_element(self):
        with self.assertRaises(Exception):
            self.lista_deseados.obtener_elemento(self.ford)

    # Operadores
    def test_total_lista_deseados(self):
        self.assertGreater(self.lista_deseados.total(), 0)
        self.assertLess(self.lista_deseados.total(), self.audi.precio+1)
        self.assertEqual(self.lista_deseados.total(), self.audi.precio)

    # AssertRegex
    def test_marca_en_codigo(self):
        self.assertRegex(self.audi.codigo_prd(), self.audi.marca)

    # falla manual y skip
    @unittest.skip("Ejemplo falla manual desactivado!")
    def test_precio_mayor_o_menor(self):
        if self.audi.precio < self.audi.precio+1:
            self.fail("El precio es muy barato")


if __name__ == '__main__':
    unittest.main()
