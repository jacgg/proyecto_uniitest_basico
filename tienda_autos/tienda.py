class Tienda:
    def __init__(self):
        self.direccion = "Lima"
        self.telefono = 989876543
        self.lista_deseados = []

    def agregar_elemento_a_lista_deseados(self, item):
        self.lista_deseados.append(item)

    def remover_elemento_de_lista_deseados(self, item):
        self.lista_deseados.remove(item)

    def contiene_elementos(self):
        return len(self.lista_deseados) > 0

    def obtener_elemento(self, item):
        if item not in self.lista_deseados:
            raise Exception("Elemento: {e}, no existe en la lista"
                            .format(e=item))
        else:
            return self.lista_deseados[self.lista_deseados.index(item) - 1]

    def total(self):
        return sum([item.precio for item in self.lista_deseados])